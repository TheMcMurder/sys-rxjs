import { interval } from 'rxjs'
import { tap, finalize } from 'rxjs/operators'

console.log('test')
setTimeout(() => {
  console.log('module 1 finalize', finalize.toString())
  const sub = interval(1000).pipe(
    tap((x) => console.log('module 1', x, finalize.toString())),
    finalize(() => {
      console.log('************************')
      console.log('module 1 finalize called')
      console.log('************************')
    })
  ).subscribe()
  setTimeout(() => {
    console.log('module 1 trigger unsubscribe on failing stream')
    sub.unsubscribe()
  }, 3000)
}, 7000)

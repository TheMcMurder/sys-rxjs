# Recreating a bug with rxjs and systemjs

## Steps to reproduce
1. run `npm i`
2. run `npx httpserver`
3. in another termainl run `npm run start`
4. Open a web browser to `http://localhost:8080`
5. Open the console and see that finalize isn't correctly called in every situation
6. Remove `setTimeout` on line 54 of index.html and notice that the failing finalize switches

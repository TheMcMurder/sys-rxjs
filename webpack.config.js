const webpack = require('webpack')
const path = require('path');

module.exports = {
  entry: {
    mod1: './src/mod1.js'
  },
  output: {
    filename: '[name].js',
    libraryTarget: 'system',
    path: path.resolve(__dirname, 'build'),
  },
  mode: 'development',
  resolve: {
    modules: [
      __dirname,
      'node_modules',
    ],
  },
  devtool: 'none',
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        loader: 'babel-loader',
      },
      {
        parser: {system: false}
      },
    ]
  },
  externals: [
    /^rxjs\/?.*$/,
  ],
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
  }
}
